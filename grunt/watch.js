module.exports = function (grunt, options) {


    return {
        js: {
            files: "src/**/*.js",
            tasks: ["babel"],
            options: {
                "spawn"         :   false,
                "debounceDelay" :   10,
                "interval"      :   10
            }
        }

    }
};