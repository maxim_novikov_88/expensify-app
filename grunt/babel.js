module.exports = function (grunt, options) {

    return {
        options: {
            sourceMap: true,
            presets: ['react', 'env']
        },
        dist: {
            files: {
                'public/scripts/core.app.js': 'src/app.js'
                // 'public/scripts/core.app.js': 'src/playground/counter-example.js'
            }
        }
    }
};