import "react-dates/initialize"; //Cannot read property 'theme' of undefined - error handler for react-dates

import 'normalize.css/normalize.css';
import "react-dates/lib/css/_datepicker.css";

import './styles/styles.scss';

import React from 'react';
import ReactDOM from 'react-dom';
import AppRouter, { history } from "./routes/AppRouter";

import { Provider } from "react-redux";

import configureStore from "./store/configureStore";
import {startSetExpenses} from "./actions/expenses";
import { login, logout } from "./actions/auth";
import getVisibleExpenses from "./selectors/expenses";

import { fireBase } from "./firebase/firebase"


const store = configureStore();

// store.dispatch(addExpense({description: "Water bill", amount: 10, createdAt: 1000}));
// store.dispatch(addExpense({description: "Gas bill", amount: 100, createdAt: -1000}));
// store.dispatch(addExpense({description: "Rent", amount: 110000}));

// store.dispatch(setTextFilter("Water"));
// setTimeout(() => {
//     store.dispatch(setTextFilter("bill"));
// }, 3000);
// const state = store.getState();
// const visibleExpenses = getVisibleExpenses(state.expenses, state.filters);
// console.log(visibleExpenses);

let hasRendered = false;
const renderApp = () => {
        if(!hasRendered){
            ReactDOM.render(jsx, document.getElementById("app"));
            hasRendered = true;
        }
    };


const jsx = (
    <Provider store={store}>
        <AppRouter />
    </Provider>
);

ReactDOM.render(<div className={"loader"}>Loading... </div>, document.getElementById("app"));


fireBase.auth().onAuthStateChanged((user) => {
   if(user){
       console.log(user.uid);
       store.dispatch(login(user.uid));
       store.dispatch(startSetExpenses()).then(() => {
           renderApp();

           if(history["location"].pathname === "/"){
               history.push("/home");
           }
       });
   }else{
       store.dispatch(logout());
       renderApp();
       history.push("/");
   }
});