

const promise = new Promise((resolve, reject) => {
    setTimeout(() => {
        // resolve("This is my resolved data");

        reject("Something whent wrong");
    }, 1500)
});

promise.then((data) => {
    console.log("Here we return another promise");
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve("this is the second promise");
        }, 1500)
    });
}).then((data) => {
    console.log("It is can return result from second promise - " + data);
}).catch((error) => {
    console.log(error)
});

//
// promise.then((data) => {
//     console.log(data);
//     return data;
// }).then((prevData) => {
//     console.log("It is run after first then if promise resolved, and return prevData if prev then return anything");
// }).catch((error) => {
//     console.log(error)
// });
