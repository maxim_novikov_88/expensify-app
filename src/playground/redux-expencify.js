






store.subscribe(() => {
    const state = store.getState(),
        visibleExpenses = getVisibleExpenses(state.expenses, state.filters);
    console.log(visibleExpenses);
});


const expenseOne = store.dispatch(addExpense({description: "Rent", amount: 1000, createdAt: -21000})),
    expenseTwo = store.dispatch(addExpense({description: "Coffee", amount: 100, createdAt: -1000}));

// store.dispatch(removeExpense({ id: expenseOne.expense.id}));



//
// store.dispatch(editExpense(expenseTwo.expense.id, {amount: 500}));
//
store.dispatch(setTextFilter("e"));
// store.dispatch(setTextFilter(""));
store.dispatch(sortByAmount());
store.dispatch(sortByDate());

//
// store.dispatch(setStartDate(0));
// store.dispatch(setStartDate());
// store.dispatch(setEndDate(999));
// store.dispatch(setEndDate());


const demoState = {
    expenses: [
        {
            id: "asdsadsada",
            description: "January Rent",
            note: "This is final payment for that address",
            amount: 54500,
            createdAt: 0
        }
    ],
    filters: {
        text: "rent",
        sortBy: "amount",
        startDate: undefined,
        endDate: undefined
    }
};


// const user = {
//     name: "Jane",
//     age: 23
// }
//
// console.log({
//     age: 17,
//     ...user,
//     location: "London",
// })

