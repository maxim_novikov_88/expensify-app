//
// Object destructuring
//
// const person = {
//     name: "Max",
//     age: 29,
//     location: {
//         city: "Smolensk",
//         temp: 30
//     }
// };
//
// const {name: firstName = "Anonymous", age} = person;
//
// console.log(`${firstName} is ${age}.`)
//
// const {city, temp: temperatuer} = person.location;
// if(city && temperatuer){
//     console.log(`It's ${temperatuer} in ${city}.`);
// }
//
const book = {
    title: "War and peace",
    autor: "Tolstoy L. N.",
    publisher: {
        // name: "Novikov"
    }
};

const {name: publisherName = "Self-Published"} = book.publisher;

console.log(publisherName);


//
// Array destructuring
//
// const address = ['18 shosse Krasninskoe', 'Smolensk', , '214000'];
//
// const [, city, country = "Planet Earth"] = address;
//
// console.log(`You are in ${city} ${country}`)

const item = ["Coffee (hot)", "100y.e.", "200y.e.", "300y.e."];

const [name = "Coffee", , mediumPrice] = item

console.log(`A medium ${name} costs ${mediumPrice}`);
