export default (expenses = []) => {
    const arr_values = expenses.map((expense) => {
        if(expense.amount !== undefined){
            return expense.amount;
        }
    });

    return arr_values.length ? arr_values.reduce((sumValues, currentValue) => {
        return sumValues + currentValue;
    }, 0) : 0 ;
};

