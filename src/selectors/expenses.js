import moment from "moment";

export default (expenses, {text, sortBy, startDate, endDate}) => {
    return expenses.filter((expense) => {

        const createdAtMoment = moment(expense.createdAt),
            startDateMatch = startDate ? startDate.isSameOrBefore(createdAtMoment, 'day') : true,
            endDateMatch = endDate ? endDate.isSameOrAfter(createdAtMoment, 'day') : true,
            textMatch = expense.description.toLowerCase().includes(text.toLowerCase());

        // console.log(startDateMatch && endDateMatch && textMatch, expense);

        return startDateMatch && endDateMatch && textMatch;
    }).sort((a, b) => {
        if( sortBy == "date"){
            return a.createdAt < b.createdAt ? 1 : -1;
        }else if( sortBy == "amount"){
            return a.amount < b.amount ? 1 : -1;
        }
    })
};
