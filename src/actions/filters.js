

export const setTextFilter = (text = "") => ({
        type: "SET_TEXT_FILTER",
        text
    }),
    sortByAmount = () => ({
        type: "SORT_BY_AMOUNT",
    }),
    sortByDate = () => ({
        type: "SORT_BY_DATE",
    }),
    setStartDate = (startDate) => ({
        type: "SET_START_DATE",
        startDate
    }),
    setEndDate = (endDate) => ({
        type: "SET_END_DATE",
        endDate
    });