import database from "./../firebase/firebase"

export const startAddExpense = (expenseData = {}) => {
        return (dispatch, getState) => {
            const {
                description = "",
                note = "",
                amount = 0,
                createdAt = 0
            } = expenseData,
            userId = getState().auth.uid;

            const expense = {description, note, amount, createdAt};

            return database.ref(`users/${userId}/expenses`)
                .push(expense)
                .then((ref) => {
                    dispatch(addExpense({
                        id: ref.key,
                        ...expense
                    }));
                })
                .catch((e) => {
                    console.log(e.message);
                })
        }
    },
    addExpense = (expense) => ({
        type: "ADD_EXPENSE",
        expense
    }),
    removeExpense = ({id} = {}) => ({
        type: "REMOVE_EXPENSE",
        id
    }),
    startRemoveExpense = ({id} = {}) => {
        return (dispatch, getState) => {
            const uid = getState().auth.uid;

            return database.ref(`users/${uid}/expenses/${id}`)
                .remove()
                .then(() => {
                    dispatch(removeExpense({id}));
                })
        }
    },
    editExpense = (id, updates) => ({
        type: "EDIT_EXPENSE",
        id,
        updates
    }),
    startEditExpenses = (id, updates) => {
        return (dispatch, getState) => {
            const uid = getState().auth.uid;

            return database.ref(`users/${uid}/expenses/${id}`)
                .update(updates)
                .then(() => {
                     dispatch(editExpense(id, updates));
                })
        }
    },
    setExpenses = (expenses) => ({
        type: "SET_EXPENSES",
        expenses
    }),
    startSetExpenses = () => {
        return (dispatch, getState) => {
            const uid = getState().auth.uid;

            return database.ref(`users/${uid}/expenses`)
                .once("value")
                .then((snapshot) => {
                    const expenses = [];
                    snapshot.forEach((childSnapshot) => {
                        expenses.push({
                            id: childSnapshot.key,
                            ...childSnapshot.val()
                        })
                    });
                    dispatch(setExpenses(expenses))
                })

        };
    };
