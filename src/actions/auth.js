import { fireBase, googleProvider } from "./../firebase/firebase"

export const login = (uid) => ({
        type: "LOGIN",
        uid
    }),
    startLogin = () => {
        return (dispatch) => {
            return fireBase.auth().signInWithPopup(googleProvider);
        }
    },
    logout = () => ({
        type: "LOGOUT"
    }),
    startLogout = () => {
        return (dispatch) => {
            return fireBase.auth().signOut();
        }
    };