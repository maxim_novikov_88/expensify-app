import moment from "moment/moment";

const expenses = [{
    id: '1',
    description: "Gum",
    note: '',
    amount: 100,
    createdAt: 0
},{
    id: '2',
    description: "Bread",
    note: '',
    amount: 1000,
    createdAt: moment(0).subtract(5, "days").valueOf()
},{
    id: '3',
    description: "Money",
    note: '',
    amount: 10000,
    createdAt: moment(0).add(5, "days").valueOf()
}];

export default expenses;