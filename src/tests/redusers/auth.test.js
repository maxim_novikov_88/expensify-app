import authReducer from "../../redusers/auth";

test("should set uid for login", () => {
    const action = {
        type: "LOGIN",
        uid: "asd"
    };

    const state = authReducer({}, action);

    expect(state.uid).toBe(action.uid);
});

test("should set uid for logout", () => {
    const action = {
        type: "LOGOUT"
    };

    const state = authReducer({uid: "asd"}, action);

    expect(state.uid).toBeFalsy();
});