import moment from "moment";
import filtersReducer from "../../redusers/filters"

test("should setup default filter values", () => {
    const state = filtersReducer(undefined, {type: "@@INIT"});

    expect(state).toEqual({
        text: "",
        sortBy: "date",
        startDate: moment().startOf("month"),
        endDate: moment().endOf("month")
    })
});

test("should set sortBy to amount", () => {

    const state = filtersReducer(undefined, { type: "SORT_BY_AMOUNT" });
    expect(state.sortBy).toBe("amount")
});

test("should set sortBy to date", () => {

    const currentState = {
            text: "",
            sortBy: "amount",
            startDate: undefined,
            endDate: undefined
        },
        action = { type: "SORT_BY_DATE" };

    const state = filtersReducer(currentState, action);

    expect(state.sortBy).toBe("date");
});

test("should set text filter object", () => {

    const text = "",
        action = {
            type: "SET_TEXT_FILTER",
            text
        },
        state = filtersReducer(undefined, action);

    expect(state.text).toBe("")
});

test("should set startDate filter object", () => {

    const startDate = moment(),
        action = {
            type: "SET_START_DATE",
            startDate
        },
        state = filtersReducer(undefined, action);

    expect(state.startDate).toEqual(startDate)
});

test("should set endDate filter object", () => {

    const endDate = moment(),
        action = {
            type: "SET_END_DATE",
            endDate
        },
        state = filtersReducer(undefined, action);

    expect(state.endDate).toEqual(endDate)
});

