import expensesReducer from "../../redusers/expenses";
import expenses from "../fixtures/expenses";
import uuid from "uuid";

test("should set default state", () => {
    const state = expensesReducer(undefined, {type: "@@INIT"});

    expect(state).toEqual([]);
});

test("should remove expense by id", () => {
    const action = {
        type: "REMOVE_EXPENSE",
        id: expenses[1].id
    };

    const state = expensesReducer(expenses, action);

    expect(state).toEqual([expenses[0], expenses[2]]);
});

test("should not remove expense by id, if id not found", () => {
    const action = {
        type: "REMOVE_EXPENSE",
        id: "asdasdasd"
    };

    const state = expensesReducer(expenses, action);

    expect(state).toEqual(expenses);
});

test("should add an expense", () => {

    const expense = {
            ...expenses[0],
            id: uuid()
        },
        action = {
            type: "ADD_EXPENSE",
            expense
        },
        state = expensesReducer(expenses, action);

    expect(state).toEqual([...expenses, expense]);
});

test("should edit an expense", () => {
    const description = "New description",
        action = {
            type: "EDIT_EXPENSE",
            id: expenses[0].id,
            updates: {description},
        },
        state = expensesReducer(expenses, action);

    expect(state[0]).toEqual({
        ...expenses[0],
        description
    })

});

test("should not edit an expense, if expense not found", () => {
    const updates = {
            description: "New description"
        },
        action = {
            type: "EDIT_EXPENSE",
            id: uuid(),
            updates,
        },
        state = expensesReducer(expenses, action);

    expect(state).toEqual(expenses);
});

test("should set expenses", () => {
    const action = {
            type: "SET_EXPENSES",
            expenses: [expenses[1]]
        },
        state =  expensesReducer(expenses, action);

    expect(state).toEqual([expenses[1]]);
});