import React from "react";
import {shallow} from "enzyme";
import ExpenseForm from "../../components/ExpenseForm";

import { DateRangePicker, SingleDatePicker, DayPickerRangeController } from 'react-dates';

import expenses from "../fixtures/expenses";
import moment from "moment";



test("should render ExpenseForm correctly", () => {
    const wrapper = shallow(<ExpenseForm/>);

    expect(wrapper).toMatchSnapshot();
});

test("should render ExpenseForm correctly with expense data", () => {
    const wrapper = shallow(<ExpenseForm {...expenses[2]}/>);

    expect(wrapper).toMatchSnapshot();
});

test("should render error for invalid from submission", () => {
    const wrapper = shallow(<ExpenseForm />);

    expect(wrapper).toMatchSnapshot();
    wrapper.find("form").simulate("submit", {
        preventDefault: () => {}
    });

    expect(wrapper.state().error.length).toBeGreaterThan(0);
    expect(wrapper).toMatchSnapshot();
});

test("should set description on input change", () => {
    const wrapper = shallow(<ExpenseForm />),
        value = "New Description";

    wrapper.find("input").at(0).simulate("change", {
        target: { value }
    });

    expect(wrapper.state().description).toBe(value);
    expect(wrapper).toMatchSnapshot();

});

test("should set note on textarea change", () => {
    const wrapper = shallow(<ExpenseForm/>),
        value = "New note text, for area";

    wrapper.find("textarea").at(0).simulate("change", {
        target: { value }
    });

   expect(wrapper.state().note).toBe(value);
   expect(wrapper).toMatchSnapshot();

});

test("should set amount if input valid", () => {
    const wrapper = shallow(<ExpenseForm />),
        value = '23.50';

    wrapper.find(".amount-field").simulate("change", {
        target: { value },
    });

    expect(wrapper.state().amount).toBe(value);
    expect(wrapper).toMatchSnapshot();
});


test("should not set amount if input invalid", () => {
    const wrapper = shallow(<ExpenseForm />),
        value = '23.505';

    wrapper.find(".amount-field").simulate("change", {
        target: { value },
    });

    expect(wrapper.state().amount).toBe("");
    expect(wrapper).toMatchSnapshot();
});


test("should call onSubmit prop for valid form submition", () => {
    const onSubmitSpy = jest.fn(),
        wrapper = shallow(<ExpenseForm expense={expenses[0]} onSubmit={onSubmitSpy}/>);

    wrapper.find("form").simulate("submit", {
        preventDefault: () => {}
    });

    expect(wrapper.state().error).toBe("");

    expect(onSubmitSpy).toHaveBeenLastCalledWith({
        description: expenses[0].description,
        amount: parseFloat(expenses[0].amount) * 100,
        note: expenses[0].note,
        createdAt: expenses[0].createdAt
    });
});

test("should set new date on date change", () => {
    const wrapper = shallow(<ExpenseForm />),
        now = moment(),
        dateChange = wrapper.find(SingleDatePicker).at(0).prop("onDateChange");

    dateChange(now);
    expect(wrapper.state().createdAt).toEqual(now);
});

test("should set calendar focused on change", () => {
    const wrapper = shallow(<ExpenseForm />),
        focused = true,
        focusChange = wrapper.find(SingleDatePicker).at(0).prop("onFocusChange");

    // wrapper.find('SingleDatePicker').at(0).prop("onFocusChange");
    // ToKnown not working because he need to find a component, not a string with name
    focusChange({focused});
    expect(wrapper.state().calendarFocused).toBe(focused);
});