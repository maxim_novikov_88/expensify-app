import React from "react";
import { shallow } from "enzyme";
import { ExpenseListFilters} from "../../components/ExpenseListFilters";
import { filters, altFilters} from "../fixtures/filters";
import { DateRangePicker } from "react-dates";
import moment from "moment"

let setTextFilter, sortByDate, sortByAmount, setStartDate, setEndDate, wrapper;
beforeEach(() => {
    setTextFilter = jest.fn();
    sortByDate = jest.fn();
    sortByAmount = jest.fn();
    setStartDate = jest.fn();
    setEndDate = jest.fn();
    wrapper = shallow(
        <ExpenseListFilters
            filters={filters}
            setTextFilter = {setTextFilter}
            sortByDate = {sortByDate}
            sortByAmount = {sortByAmount}
            setStartDate = {setStartDate}
            setEndDate = {setEndDate}
        />
    )
});

test("should render ExpenseListFilters correctly", () => {
    expect(wrapper).toMatchSnapshot();
});

test("should render ExpenseListFilters with alt data correctly", () => {
    expect(wrapper.setProps({filters: altFilters})).toMatchSnapshot();
});

test("should handle text change", () => {
    const value = "sortTextStr";

    wrapper.find(".filter-by-text").simulate("change", {
        target: {value}
    });

    expect(setTextFilter).toHaveBeenLastCalledWith(value);
});

test("should sort by date", () => {
    const value = "date";
    wrapper.setProps({filters: altFilters});

    wrapper.find("select").simulate("change", {
        target: { value }
    });

    expect(sortByDate).toHaveBeenCalled();
});

test("should sort by amount", () => {
    const value = "amount";

    wrapper.find("select").simulate("change", {
        target: { value }
    });

    expect(sortByAmount).toHaveBeenCalled();
});

test("should handle date changes", () => {
    const startDate = moment(0).add(2, "days"),
        endDate = moment(0).add(5, "days"),
        changeDate = wrapper.find(DateRangePicker).prop("onDatesChange");

    changeDate({ startDate, endDate });

    expect(setStartDate).toHaveBeenCalledWith(startDate);
    expect(setEndDate).toHaveBeenCalledWith(endDate);


});

test("should handle date focus changes", () => {
    const calendarFocused = "endDate",
        focusChange = wrapper.find(DateRangePicker).prop("onFocusChange");

    focusChange(calendarFocused);
    expect(wrapper.state().calendarFocused).toBe(calendarFocused);
});

