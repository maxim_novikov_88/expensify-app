import React from "react";
import {shallow} from "enzyme";
import HomePage from "../../../components/page/Home";

// ToKnown not working
// FAIL  src/tests/components/page/Home.test.js
//   ● Test suite failed to run
//
// TypeError: Cannot read property 'theme' of undefined
//
// 4 | import { setTextFilter } from "./../actions/filters"
// 5 | import { sortByAmount, sortByDate, setStartDate, setEndDate } from "../actions/filters";
// > 6 | import { DateRangePicker } from "react-dates";

// replace to import { DateRangePicker, SingleDatePicker, DayPickerRangeController } from 'react-dates'; (v-16.2.0)

// 7 |
// 8 | class ExpenseListFilters extends React.Component {
//     9 |     state = {
//
//         at Object.create (node_modules/react-with-styles/lib/ThemedStyleSheet.js:33:27)
//     at withStyles (node_modules/react-with-styles/lib/withStyles.js:78:58)
//     at Object.<anonymous> (node_modules/react-dates/lib/components/CalendarDay.js:291:54)
// at Object.<anonymous> (node_modules/react-dates/lib/index.js:5:20)
// at Object.<anonymous> (node_modules/react-dates/index.js:2:18)
// at Object.<anonymous> (src/components/ExpenseListFilters.js:6:19)
// at Object.<anonymous> (src/components/page/Home.js:3:27)
// at Object.<anonymous> (src/tests/components/page/Home.test.js:3:13)


test("should render HomePage correctly", () => {
    const wrapper = shallow(<HomePage />);
    expect(wrapper).toMatchSnapshot();
});