import React from "react";
import { shallow } from "enzyme";
import { EditExpensePage } from "../../../components/page/Edit";

import ExpenseForm from "../../../components/ExpenseForm";
import expenses from "../../fixtures/expenses";


let wrapper, startEditExpenses, startRemoveExpense, history, expense;
beforeEach(() => {
    startEditExpenses = jest.fn();
    startRemoveExpense = jest.fn();
    history = { push: jest.fn() };
    wrapper = shallow(
        <EditExpensePage
            startEditExpenses={startEditExpenses}
            startRemoveExpense={startRemoveExpense}
            history={history}
            expense={expenses[1]}
        />
    );
});

test("should render EditExpensePage correctly", () => {
    expect(wrapper).toMatchSnapshot();
});

test("should handle startEditExpenses correctly", () => {
    const submit = wrapper.find(ExpenseForm).prop("onSubmit");

    // console.log(submit, expense);
    submit(expenses[1]);
    expect(history.push).toHaveBeenCalledWith("/");
    expect(startEditExpenses).toHaveBeenCalledWith(expenses[1].id, expenses[1]);
});

test("should handle removeExpense correctly", () => {
    wrapper.find("button.remove-expense").simulate("click");

    expect(history.push).toHaveBeenCalledWith("/");
    expect(startRemoveExpense).toHaveBeenCalledWith({id: expenses[1].id});

});