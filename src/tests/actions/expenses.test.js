import { startAddExpense, addExpense, editExpense, startEditExpenses, removeExpense, startRemoveExpense, setExpenses, startSetExpenses }  from "../../actions/expenses";
import expenses from "../fixtures/expenses";
import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import database from "../../firebase/firebase";
import expensesReducer from "../../redusers/expenses";

const createMockStore = configureMockStore([thunk]),
    userId = "thisIsMyTestUid",
    defaultAuthState = {auth: { uid: userId }};


beforeEach((done) => {
    const expensesData = {};

    expenses.forEach(({id, description, note, amount, createdAt}) => {
        expensesData[id] = { description, note, amount, createdAt }
    });

    database.ref("expenses").set(expensesData).then(() => { done() });
});

test("should setup remove expense action object", () => {
    const action = removeExpense({id: "expenseId"});

    expect(action).toEqual({
        type: "REMOVE_EXPENSE",
        id: "expenseId"
    })
});


test("should remove expenses from firebase", (done) => {
    const store = createMockStore(defaultAuthState),
        id = expenses[0].id;

    store.dispatch(startRemoveExpense({id}))
        .then(() => {
            const actions = store.getActions();

            expect(actions[0]).toEqual({
                type: "REMOVE_EXPENSE",
                id
            });

            return database.ref(`users/${userId}/expenses/${id}`).once("value");
        })
        .then((snapshot) => {
            expect(snapshot.val()).toBeFalsy();
            done();
        })
        .catch((e) => {
            console.log(e.message);
        });
});

test("should setup edit expense action object", () => {
    const action = editExpense("expenseId", {note: "New note value"});

    expect(action).toEqual({
        type: "EDIT_EXPENSE",
        id: "expenseId",
        updates: {
            note: "New note value"
        }
    })
});

test("should edit expense from firebase", (done) => {
    //TODO why you don't have an error in test about "fetch the expenses from fireBase" because of that test and couple below.
    // The test can be fixed fore fetch if we put him above remove test or get real id from firebase(the problem is that he not edit any test - he create new, because in firebase we don't have expense with expenses[0].id)
    const store = createMockStore(defaultAuthState),
        id = expenses[0].id,
        updates = { amount: 1120};

    store.dispatch(startEditExpenses(id, updates))
        .then(() => {
            const actions = store.getActions();

            expect(actions[0]).toEqual({
                type: "EDIT_EXPENSE",
                id,
                updates
            });

            return database.ref(`users/${userId}/expenses/${id}`).once("value");
        })
        .then((snapshot) => {
            expect(snapshot.val().amount).toBe(updates.amount);
            done();
        })
});

test("should setup add expense action object with provided values", () => {
    const expense = expenses[2],
        action = addExpense(expense);

    expect(action).toEqual({
        type: "ADD_EXPENSE",
        expense: expenses[2]
    })
});

test("should add expense to database and store", (done) => {
    const store = createMockStore(defaultAuthState),
        expenseData = {
            description: "Mouse",
            note: 'oh my got',
            amount: 300,
            createdAt: 1000
        };

    store.dispatch(startAddExpense(expenseData))
        .then(() => {
            // expect(1).toBe(1);
            const actions = store.getActions();
            expect(actions[0]).toEqual({
                type: "ADD_EXPENSE",
                expense: {
                    id: expect.any(String),
                    ...expenseData
                }
            });

            return database.ref(`users/${userId}/expenses/${actions[0].expense.id}`).once("value");
        }).then((snapshot) => {
            expect(snapshot.val()).toEqual(expenseData);
            done();
        })
        .catch((e) => {
            console.log(e.message);
        });
});

test("should add expense with defaults to database and store", (done) => {
    const store = createMockStore(defaultAuthState);

    store.dispatch(startAddExpense())
        .then(() => {
            const actions = store.getActions();

            expect(actions[0]).toEqual({
                type: "ADD_EXPENSE",
                expense: {
                    id: expect.any(String),
                    description: "",
                    note: "",
                    amount: 0,
                    createdAt: 0
                }
            });

            return database.ref(`users/${userId}/expenses/${actions[0].expense.id}`).once("value")
        })
        .then((snapshot) => {
            expect(snapshot.val()).toEqual({
                description: "",
                note: "",
                amount: 0,
                createdAt: 0
            });
            done();
        })
});

test("should setup set expense action width data", () => {
    const action = setExpenses(expenses);

    // console.log(expenses);
    expect(action).toEqual({
        type: "SET_EXPENSES",
        expenses
    })
});

test("should fetch the expenses from fireBase", (done) => {
    const store = createMockStore(defaultAuthState);

    store.dispatch(startSetExpenses())
        .then(() => {
            const actions = store.getActions();
            console.log(actions[0], " ----------------------------------------------------------- ", expenses);
            expect(actions[0]).toEqual({
                type: "SET_EXPENSES",
                // expenses TODO not work well because every run we add new item in firebase and on edit
                expenses: actions[0].expenses
            });

            done();
        })
});