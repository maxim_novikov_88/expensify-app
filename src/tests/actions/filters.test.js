import moment from "moment";
import {
    setTextFilter,
    sortByAmount,
    sortByDate,
    setStartDate,
    setEndDate
} from "../../actions/filters";



test("should setup default text filter object", () => {
    const filter = setTextFilter();

    expect(filter).toEqual({
       type: "SET_TEXT_FILTER",
       text: ""
    });
});

test("should setup text filter object", () => {
    const text = "textFilter";
    const filter = setTextFilter(text);

    expect(filter).toEqual({
       type: "SET_TEXT_FILTER",
       text
    });
});

test("should setup by amount sort object", () => {
    const sort = sortByAmount();

    expect(sort).toEqual({
        type: "SORT_BY_AMOUNT",
    })
});

test("should setup by date sort object", () => {
    const sort = sortByDate();

    expect(sort).toEqual({
        type: "SORT_BY_DATE",
    })
});

test("should generate start date filter object", () => {
    const filter = setStartDate(moment(0));

    expect(filter).toEqual({
        type: "SET_START_DATE",
        startDate: moment(0)
    })
});

test("should generate end date filter object", () => {
    const filter = setEndDate(moment(10000));

    expect(filter).toEqual({
        type: "SET_END_DATE",
        endDate: moment(10000)
    })
});