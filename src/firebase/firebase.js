import fireBase from 'firebase/app';
import 'firebase/database';
import 'firebase/auth';

import moment from "moment";

// console.log(fireBase);
// Initialize Firebase
const config = {
    apiKey: process.env.FIREBASE_API_KEY,
    authDomain: process.env.FIREBASE_AUTH_DOMAIN,
    databaseURL: process.env.FIREBASE_DATABASE_URL,
    projectId: process.env.FIREBASE_PROJECT_ID,
    storageBucket: process.env.FIREBASE_STORAGE_BUCKET,
    messagingSenderId: process.env.FIREBASE_MESSAGING_SENDER_ID
};

fireBase.initializeApp(config);
const database = fireBase.database(),
    googleProvider = new fireBase.auth.GoogleAuthProvider();

export {fireBase, googleProvider, database as default};


// Helpers
// database.ref("expenses").on("child_changed", (childSnapshot) => {
//     console.log(childSnapshot.key, childSnapshot.val())
// });
//
// database.ref("expenses").on("child_added", (childSnapshot) => {
//     console.log(childSnapshot.key, childSnapshot.val())
// });
//
// database.ref("expenses").on("child_removed", (snapshot) => {
//     console.log(snapshot.key, snapshot.val())
// });

// database.ref("expenses")
//     .on("value", (snapshot) => {
//         const expenses = [];
//         snapshot.forEach((childSnapshot) => {
//             expenses.push({
//                 ...childSnapshot.val(),
//                 id: childSnapshot.key
//             })
//         });
//
//         console.log(expenses);
//     });
//
//
// setTimeout(() => {
//     database.ref("expenses").push({
//         description: "BubleGum",
//         note: '',
//         amount: 1000,
//         createdAt: 15880
//     })
// }, 10000);

// database.ref("expenses")
//     .once("value")
//     .then((snapshot) => {
//         const expenses = []
//         snapshot.forEach((childSnapshot) => {
//             expenses.push({
//                 ...childSnapshot.val(),
//                 id: childSnapshot.key
//             })
//         })
//
//         console.log(expenses);
//     })
//     .catch((e) => {
//        console.log(e.message)
//     });

// const expenses = [{
//     description: "Gum",
//     note: '',
//     amount: 100,
//     createdAt: 0
// },{
//     description: "Bread",
//     note: '',
//     amount: 1000,
//     createdAt: moment(0).subtract(5, "days").valueOf()
// },{
//     description: "Money",
//     note: '',
//     amount: 10000,
//     createdAt: moment(0).add(5, "days").valueOf()
// }];
//
// expenses.forEach((expense) => {
//     database.ref("expenses").push(expense)
// });

//
// database.ref("notes/-LNzwMnPusB8Glr4Gwd5").update({
//     body: "Lets drink a cup of tea"
// });

// database.ref("notes")
//     .push({
//         id: "13",
//         body: "this is my note",
//         title: "note 2"
//     });

// database.ref("notes").set(notes);
//
// database.ref("notes/12");

// database.ref()
//     .once("value")
//     .then((snapshot) => {
//         const value = snapshot.val();
//         console.log(value);
//     })
//     .catch((error) => {
//         console.log(error)
//     });

// const onValueChange = database.ref()
//     .on("value",
//         (snapshot) => {
//             console.log(snapshot.val())
//         },
//         (e) => {
//             console.log(e.message);
//         });
//
// setTimeout(() => {
//     database.ref().update({
//         "age": 32,
//     });
// }, 4500);
//
// setTimeout(() => {
//     database.ref().update({
//         "name": "Maxim",
//         "location/city": "Seatle",
//     });
// }, 8500);
//
// setTimeout(() => {
//     database.ref().off("value", onValueChange);
// }, 10000);
//
// setTimeout(() => {
//     database.ref().update({
//         "location/city": "Moscow",
//     });
// }, 12500);

// database.ref().set({
//     name: "Maksim Novikov",
//     age: 30,
//     stressLevel: 6,
//     job: {
//         title: "Software developer",
//         company: "Google"
//     },
//     isSingle: false,
//     location: {
//         city: "Smolensk",
//         country: "Russia"
//     }
// }).then(() => {
//     console.log("data is saved");
// }).catch((error) => {
//     console.log(error.message);
// });

// database.ref().set("This is some data");
// database.ref('age').set(31);
// database.ref('location/city').set("Moscow");

// database.ref("attributes").set({
//     "height": 180,
//     "weight": 66
// }).then(() => {
//     console.log("data is saved")
// }).catch((error) => {
//     console.log(error.message)
// });


// database.ref().remove().then(() => {
//     console.log("property was removed");
// }).catch((error) => {
//     console.log(error.message);
// });

// database.ref("isSingle").set(null);
//
// database.ref().update({
//     stressLevel: 9,
//     "job/company": "Yandex",
//     "location/city": "Moscow"
// });