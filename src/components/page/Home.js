import React from "react";
import ExpenseList from "../ExpenseList";
import ExpenseListFilters from "../ExpenseListFilters";


const ExpenseDashboardPage = () => (
    <div className={"fix-width-wrap expenses-content"}>
        <ExpenseListFilters />
        <ExpenseList />
    </div>
);

export default ExpenseDashboardPage;