import React, { Component } from "react";
import { connect } from "react-redux";
import { startLogin } from "./../../actions/auth"

export class LoginPage extends Component{

    onLoginHandler = () => {
        this.props.startLogin();
    };

    render(){
        return(
            <div className={"login-page"}>
                <div className={"middle-box"}>
                    <h1>EXPENSIFY!</h1>
                    <p>It's time to get your expenses under control!</p>
                    <button onClick={this.onLoginHandler}>Login with Google</button>
                </div>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => ({
    startLogin: () => dispatch(startLogin())
});

export default connect(null, mapDispatchToProps)(LoginPage);

