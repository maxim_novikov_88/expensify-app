import React from "react";
import {connect} from "react-redux";
import ExpenseForm from "../ExpenseForm"
import { editExpense, startEditExpenses, removeExpense, startRemoveExpense } from "../../actions/expenses";


export class EditExpensePage extends React.Component{
    onSubmit = (expense) => {
        console.log(expense);
        this.props.startEditExpenses(this.props.expense.id, expense);
        this.props.history.push("/");
    };

    onClick = () => {
        this.props.startRemoveExpense({id : this.props.expense.id});
        this.props.history.push("/");
    };

    render() {
        return (
            <div className={"fix-width-wrap"}>
                <ExpenseForm
                    expense={this.props.expense}
                    onSubmit={this.onSubmit}
                    isEditForm={true}
                />
                <button
                    className={"remove-expense"}
                    onClick={this.onClick}
                >Remove</button>
            </div>
        )
    }
}

const mapStateProps = (state, props) => {
    return {
        expense: state.expenses.find((expense) => {
            return expense.id === props.match.params.id
        })
    }
};

const mapDispatchToProps = (dispatch, props) => {
    return {
        startEditExpenses: (expenseId, expense) => dispatch(startEditExpenses(expenseId, expense)),
        startRemoveExpense: (data) => dispatch(startRemoveExpense(data))
    }
};

export default connect(mapStateProps, mapDispatchToProps)(EditExpensePage);
