import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { startLogout } from "../../actions/auth";
import ExpensesSummary from "../ExpensesSummary";

export const Header = ({ startLogout, ...restProps }) => {

    return (
        <header>
            <div className={"top-panel"}>
                <div className={"fix-width-wrap"}>
                    <div className={"flex-wrap"}>
                        <Link className={"main-col"} to="/home"><h1>EXPENSIFY</h1></Link>
                        {/*<NavLink to="/help" activeClassName="is-active">Help</NavLink>*/}
                        <a href={"javascript:;"} className={"logout-btn secondary-col"} onClick={startLogout}>Logout</a>
                    </div>
                </div>
            </div>

            <div className={"fix-width-wrap"}>
                {
                    restProps.location.pathname.indexOf("/home") !== -1 &&
                    <div>
                        <ExpensesSummary />
                        <Link to="/create"><button>Add Expense</button></Link>
                    </div>
                }

                {
                    restProps.location.pathname.indexOf("/edit") !== -1 &&
                    <h3>Edit Expense</h3>
                }

                {
                    restProps.location.pathname.indexOf("/create") !== -1 &&
                    <h3>Add Expense</h3>
                }
            </div>
        </header>
    );
}

const mapDispatchToProps = (dispatch) => ({
    startLogout: () => dispatch(startLogout())
});

export default connect(null, mapDispatchToProps)(Header);