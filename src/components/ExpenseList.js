import React from "react";
import { connect } from "react-redux";
import ExpenseListItem from "./ExpenseListItem";
import selectExpense from "./../selectors/expenses"

export const ExpenseList = (props) => (
    <div className={"expense-list"}>
        <div className={"flex-wrap expense-title"}>
            <span className={"only-for-mob"}>Expenses</span>
            <span className={"main-col"}>Expense</span>
            <span className={"secondary-col"}>Amount</span>
        </div>

        <ul>
            {
                props.expenses.length === 0 ? (
                    <li>No expenses</li>
                ) : (
                    props.expenses.map((expense, i) => {
                        return <ExpenseListItem {...expense} key={expense.id}/>
                    })
                )
            }
        </ul>
    </div>
);

const mapStateProps = (state) => {
    return {
        expenses: selectExpense(state.expenses, state.filters),
        // filters: state.filters
    }
};

export default connect(mapStateProps)(ExpenseList);
