import uuid from "uuid";
import React from "react";
import { connect } from "react-redux";
import { setTextFilter } from "./../actions/filters"
import { sortByAmount, sortByDate, setStartDate, setEndDate } from "../actions/filters";
import { DateRangePicker } from "react-dates";

export class ExpenseListFilters extends React.Component {
    state = {
        calendarFocused: null
    };

    onDatesChange = ({startDate, endDate}) => {
        this.props.setStartDate(startDate);
        this.props.setEndDate(endDate);
    };

    onFocusChange = (calendarFocused) => {
        this.setState(() => ({
            calendarFocused
        }));
    };

    onTextChange = (e) => {
        this.props.setTextFilter(e.target.value);
    };

    onSortChange = (e) => {
        const value = e.target.value;

        if(value == "amount"){
            this.props.sortByAmount();
        }else if(value == "date"){
            this.props.sortByDate();
        }
    };

    render() {
        return (
            <div className={"expenses-filter"}>

                <div className={"form-control input"}>
                    <input
                        type="text"
                        value={this.props.filters.text}
                        placeholder={"Search expenses"}
                        onChange={this.onTextChange}
                        className={"filter-by-text"}
                    />
                </div>

                <div className={"form-control select"}>
                    <select
                        value={this.props.filters.sortBy}
                        onChange={this.onSortChange}>
                        <option value="date">Date</option>
                        <option value="amount">Amount</option>
                    </select>
                </div>

                <DateRangePicker
                    startDate={this.props.filters.startDate}
                    startDateId={uuid()}
                    endDate={this.props.filters.endDate}
                    endDateId={uuid()}
                    onDatesChange={this.onDatesChange}
                    focusedInput={this.state.calendarFocused}
                    onFocusChange={this.onFocusChange}
                    showClearDates={true}
                    numberOfMonths={1}
                    isOutsideRange={() => false}
                />
            </div>
        )
    }
}

const mapStateProps = (state) => {
    return {
        filters: state.filters
    }

};

const mapDispatchToProps = (dispatch) => {
    return {
        setStartDate: (startDate) => dispatch(setStartDate(startDate)),
        setEndDate: (endDate) => dispatch(setEndDate(endDate)),
        setTextFilter: (text) => dispatch(setTextFilter(text)),
        sortByAmount: () => dispatch(sortByAmount()),
        sortByDate: () => dispatch(sortByDate())
    }
};

export default connect(mapStateProps, mapDispatchToProps)(ExpenseListFilters);