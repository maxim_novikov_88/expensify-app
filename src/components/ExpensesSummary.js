import React from "react";
import { connect } from "react-redux";
import getExpensesTotal from "../selectors/expenses-total";
import selectExpense from "../selectors/expenses";
import numeral from "numeral";

export class ExpensesSummary extends React.Component {

    render() {
        const expensesTotal = numeral(getExpensesTotal(this.props.expenses)/100).format("$0,0.00"),
            totalHtml = (!this.props.expenses.length ? 'List is empty' : (' Viewing <bold>' + this.props.expenses.length + '</bold> expense' + (this.props.expenses.length > 1 ? "s" : "" ) + ' totalling <bold>' + expensesTotal + "</bold>"));

        return (
            <p className={"total-text"} dangerouslySetInnerHTML={{__html: totalHtml }} />
        );
    }
}

const mapStateProps = (state) => {
    return {
        expenses: selectExpense(state.expenses, state.filters)
    }
};

export default connect(mapStateProps)(ExpensesSummary);


