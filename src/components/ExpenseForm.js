import React from "react";
import moment from "moment";
// import { SingleDatePicker } from "react-dates";
import { DateRangePicker, SingleDatePicker, DayPickerRangeController } from 'react-dates';

// const date = new Date();
// const now = moment();
// console.log(now.format("MMM Do, YYYY"));

export default class ExpenseForm extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            description: props.expense? props.expense.description : "",
            amount: props.expense ? props.expense.amount : "",
            note: props.expense ? props.expense.note : "",
            createdAt: props.expense ? moment(props.expense.createdAt) : moment(),
            calendarFocused: false,
            isEditForm: props.isEditForm ? props.isEditForm : false,
            error: ""
        };
    }


    onDescriptionChange = (e) => {
        const description = e.target.value;

        this.setState(() => ({description}));
    };

    onAmountChange = (e) => {
        const amount = e.target.value;

        if(!amount || amount.match(/^\d{1,}(\.\d{0,2})?$/)){
            this.setState(() => ({amount}));
        }
    };

    onNoteChange = (e) => {
        const note = e.target.value;

        this.setState(() => ({note}))
    };

    onDateChange = (createdAt) => {
        if(createdAt){


            this.setState(() => ({
                createdAt
            }))
        }
    };

    onFocusChange = ({focused}) => {
        this.setState(() => ({
            calendarFocused: focused
        }))
    };

    onSubmit = (e) => {
        e.preventDefault();

        if(!this.state.description || !this.state.amount){
            this.setState(() => ({
                error: "Please provide description and amount."
            }))
        }else{
            if(this.state.error){
                this.setState(() => ({
                  error: ""
                }));
            }

            this.props.onSubmit({
                description: this.state.description,
                amount: parseFloat(this.state.amount) * 100,
                createdAt: this.state.createdAt.valueOf(),
                note: this.state.note
            })
        }
    };

    render () {
        return (
            <div className={"expenses-form"}>
                { this.state.error &&
                    <p className="error">{this.state.error}</p>
                }
                <form onSubmit={this.onSubmit}>
                    <div className={"form-control input"}>
                        <input
                            type="text"
                            placeholder="Description"
                            autoFocus={true}
                            value={this.state.description}
                            onChange={this.onDescriptionChange}
                        />
                    </div>

                    <div className={"form-control input"}>
                        <input
                            type="text"
                            placeholder="Amount"
                            value={this.state.amount}
                            onChange={this.onAmountChange}
                            className={"amount-field"}
                        />
                    </div>

                    <div className={"form-control input"}>
                        <SingleDatePicker
                            date={this.state.createdAt}
                            onDateChange={this.onDateChange}
                            focused={this.state.calendarFocused}
                            onFocusChange={this.onFocusChange}
                            numberOfMonths={1}
                            isOutsideRange={() => false}
                        />
                    </div>

                    <div className={"form-control input"}>
                        <textarea
                            placeholder="Add a note for your expense(optional)"
                            cols="30" rows="10"
                            value={this.state.note}
                            onChange={this.onNoteChange}
                        />
                    </div>

                    <button>{ this.state.isEditForm ? "Edit Expense" : "Add Expense"}</button>
                </form>
            </div>
        )
    }
};