import React from "react";
import { Link } from "react-router-dom";
import moment from "moment";
import numeral from "numeral";

const ExpenseListItem = ({id, description, amount, createdAt}) => (
    <li className={"flex-wrap"}>
        <div className={"main-col"}>
            <Link to={`/edit/${id}`}>
                <h3>{description}</h3>
            </Link>
            <p>{moment(createdAt).format("MMMM Do, YYYY")}</p>
        </div>
        <div className={"secondary-col"}>
            <h3>{numeral(amount / 100).format('$0,0.00')}</h3>
        </div>
    </li>
);

export default ExpenseListItem;