import React from "react";
import { Router, Route, Switch } from 'react-router-dom';

import createHistory from 'history/createBrowserHistory'

import ExpenseDashboardPage from "../components/page/Home";
import LoginPage from "../components/page/Login";
import AddExpensePage from "../components/page/Create";
import EditExpensePage from "../components/page/Edit";
import HelpPage from "../components/page/Help";
import NotFoundPage from "../components/page/404";

import PrivateRoute from "./PrivateRoute";
import PublicRoute from "./PublicRoute";

export  const history = createHistory();


const AppRouter = () => (
    <Router history={history}>
        <div>
            <Switch>
                <PublicRoute path="/" component={LoginPage} exact={true}/>

                <PrivateRoute path="/home" component={ExpenseDashboardPage} exact={true}/>

                <PrivateRoute path="/create" component={AddExpensePage}/>

                <PrivateRoute path="/edit/:id" component={EditExpensePage}/>

                <PrivateRoute path="/help" component={HelpPage}/>

                <Route component={NotFoundPage} />
            </Switch>
        </div>
    </Router>
);

export default AppRouter;

