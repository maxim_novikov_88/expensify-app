import React from "react";

import { connect } from "react-redux";
import { Route, Redirect } from "react-router-dom";
import Header from "../components/layout/Header";

export const PrivateRoute = ({
     isAuthenticated,
     component: Component,
    ...restProps
}) => {
    return (
        <Route {...restProps} component={(props) => {
            if(isAuthenticated){
                return (
                    <div>
                        <Header {...restProps}/>
                        <Component {...props}/>
                    </div>
                )
            }else {
                return <Redirect to={"/"}/>
            }
        }}/>
    )
};

const mapStateToProps = (state) => ({
    isAuthenticated: !!state.auth.uid
});

export default connect(mapStateToProps)(PrivateRoute);